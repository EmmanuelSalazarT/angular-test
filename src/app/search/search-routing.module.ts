import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { WordComponent } from './word/word.component';
import { SearchResolver } from './searchresolve.resolve';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: ':word',
    component: WordComponent,
    resolve: {
      posts: SearchResolver,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),HttpClientModule],
  exports: [RouterModule]
})

export class SearchRoutingModule { }
