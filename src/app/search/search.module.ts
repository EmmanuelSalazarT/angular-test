import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';

import { SearchRoutingModule } from './search-routing.module';
import { WordComponent } from './word/word.component';
import { SearchrequestService } from './searchrequest.service';
import { SearchResolver } from './searchresolve.resolve';

@NgModule({
  declarations: [WordComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    HttpClientModule
  ],
  providers: [SearchrequestService,SearchResolver]
})
export class SearchModule { }
