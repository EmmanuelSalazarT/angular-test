import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map,filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class SearchrequestService {

  constructor(protected httpClient: HttpClient) { }

  search(word){
    return this.httpClient.get<any>("https://jsonplaceholder.typicode.com/posts/")
     .pipe(
       map(posts => posts.filter(post => 
        {
          if (post.body.includes(word))
          {
            return post;
          }
        }
       )),
     );
  }
}