import { TestBed } from '@angular/core/testing';

import { SearchrequestService } from './searchrequest.service';

describe('SearchrequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchrequestService = TestBed.get(SearchrequestService);
    expect(service).toBeTruthy();
  });
});
