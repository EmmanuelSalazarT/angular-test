import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent implements OnInit {
  public word;
  public posts;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.word = this.route.snapshot.paramMap.get("word");
    this.posts =  this.route.snapshot.data['posts'];
  }

}
