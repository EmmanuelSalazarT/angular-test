import { Injectable } from '@angular/core';
import { SearchrequestService } from './searchrequest.service';

import { Resolve, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class SearchResolver implements Resolve<any> {
  constructor(private searchrequest: SearchrequestService) {}

  resolve(route: ActivatedRouteSnapshot) : Observable<any> {
    return this.searchrequest.search(route.params.word);
  }
}