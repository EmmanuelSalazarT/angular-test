import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { FetchallComponent } from './post/fetchall/fetchall.component';
//import { FindComponent } from './post/find/find.component';
//import { SearchComponent } from './post/search/search.component';

const routes: Routes = [
  {
    path: 'busqueda',
    loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
  },
  {
    path: '',
    loadChildren: () => import('./post/post.module').then(m => m.PostModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
