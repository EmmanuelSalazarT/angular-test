import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from "@angular/router";
import { RequestsService } from '../requests.service';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.css']
})
export class FindComponent implements OnInit {
  public post = {};
  public post_id;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private requestService : RequestsService) {

  }

  ngOnInit() {
    this.post_id = this.route.snapshot.paramMap.get("id");
    this.requestService.getPost(this.post_id)
      .subscribe(Response => (this.post = Response), Error => (alert("No data")));
  }

}
