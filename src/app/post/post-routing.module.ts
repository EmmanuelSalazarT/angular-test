import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindComponent } from './find/find.component';
import { FetchallComponent } from './fetchall/fetchall.component';

const routes: Routes = [
    {
        path: ':id',
        component: FindComponent,
    },
    {
        path: '',
        component: FetchallComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PostRoutingModule { }
