import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { RequestsService } from '../requests.service';


@Component({
  selector: 'app-fetchall',
  templateUrl: './fetchall.component.html',
  styleUrls: ['./fetchall.component.css']
})
export class FetchallComponent implements OnInit {
  public posts : Observable<any>;

  constructor(private httpClient: HttpClient, private requestService : RequestsService) {

  }

  ngOnInit() {
    this.posts = this.requestService.getPosts().pipe(map(this.capitalizeTitlePosts));
  }

  private capitalizeTitlePosts = (Response) => {
    Response.forEach(element => {
      element.title = this.capitalizeStrings(element.title)
    });
    return Response;
  }

  private capitalizeStrings(str)
  {
    let stringCapitalized = "";
    let arrayStrings = str.split(" ");
    for(let i=0;i<arrayStrings.length;i++)
    {
      let word = arrayStrings[i];
      stringCapitalized+=" "+this.capitalize(word);
    }
    return stringCapitalized;
  }

  private capitalize = (word) => {
    if (typeof word !== 'string') return ''
    return word.charAt(0).toUpperCase() + word.slice(1)
  }
  

}
