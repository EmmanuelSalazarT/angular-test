import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private httpClient: HttpClient) { }

  public getPosts = () => {
    return this.httpClient
      .get("https://jsonplaceholder.typicode.com/posts");
  }

  public getPost = (id) => {
    return this.httpClient
      .get("https://jsonplaceholder.typicode.com/posts/"+id);
  }

}
