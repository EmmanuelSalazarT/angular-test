import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { PostRoutingModule } from './post-routing.module';
import { FetchallComponent } from './fetchall/fetchall.component';
import { FindComponent } from './find/find.component';
import { RequestsService } from './requests.service';

@NgModule({
  declarations: [FetchallComponent, FindComponent],
  imports: [
    CommonModule,
    PostRoutingModule,
    HttpClientModule
  ],
  exports:[FetchallComponent, FindComponent],
  providers: [RequestsService]
})
export class PostModule { }
